from flask import Flask, jsonify
import random

# create Flask app
app = Flask(__name__)

# define function for root url and return Hello World
@app.route('/')
def hello_world():
    return 'Hello world!'

# define function for /nextfunc url
@app.route('/nextfunc')
def nextfunc():
    # create return value with a func name (between func0 and func3) with led color value (red, green and blue between 0 and 32)
    returnvalue = {'funcname': 'test{0}'.format(random.randrange(4)), 'values':[random.randrange(33),random.randrange(33),random.randrange(33)]}
    # return returnvalue object in json
    return jsonify(returnvalue)

if __name__ == '__main__':
    app.run()
